package InterGallactic;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MetalCurrencyUnitMapper {
	private static Map<String, Double> MetalCurrencyUnitMap;
	private GalacticNumerals galacticNumeral;
	private RomanNumerals rNumeral;
	Pattern pattern;

	MetalCurrencyUnitMapper(RomanNumerals rNumeral,
			GalacticNumerals galacticNumeral) {
		MetalCurrencyUnitMap = new HashMap<String, Double>();
		this.rNumeral = rNumeral;
		this.galacticNumeral = galacticNumeral;
	}

	public void addCurrency(String name, double value) {
		MetalCurrencyUnitMap.put(name, value);
	}

	public double getUnitValue(String currencyName) {
		return MetalCurrencyUnitMap.get(currencyName);
	}

	public void processCreditInfo(String line) {
		String regex = GlobalConstants.regexForCreditsInfo;
		pattern = Pattern.compile(regex);
		Matcher m = pattern.matcher(line);
		m.matches();

		String[] galacticNumbers = m.group(1).split("\\s");
		String currencyName = m.group(2);
		int creditValue = Integer.parseInt(m.group(3));

		// Check if Galactic numerals used are declared first.
		if (!galacticNumeral.areValid(galacticNumbers)) {
			PrintUtils
					.promptUser("Undeclared Galactic number was used, input ignored.");
			return;
		}

		String romanNumerals = galacticNumeral.toRomanNumeral(galacticNumbers);

		float divisor = rNumeral.evaluate(romanNumerals);

		double unitValue = creditValue / divisor;

		addCurrency(currencyName, unitValue);
	}

}
