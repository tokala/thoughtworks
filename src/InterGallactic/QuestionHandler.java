package InterGallactic;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class QuestionHandler
{
	private RomanNumerals rNumeral;
	private GalacticNumerals galacticNumeral;
	private GalacticRomanMapper galacticRomanMapper;
	private MetalCurrencyUnitMapper metalCurrencyUnitMapper;
	
	Pattern pattern;
	QuestionHandler(RomanNumerals rNumeral, GalacticNumerals galacticNumeral, GalacticRomanMapper galacticRomanMapper , MetalCurrencyUnitMapper metalCurrencyUnitMapper )
	{
		this.rNumeral = rNumeral;
		this.galacticNumeral = galacticNumeral;
		this.galacticRomanMapper = galacticRomanMapper;
		this.metalCurrencyUnitMapper = metalCurrencyUnitMapper;
	}
	public String handle(String line, GlobalConstants.LineType type)
	{
		String result="";
		if(type == GlobalConstants.LineType.QUESTION_OF_TYPE_HOW_MANY)
		{
			result = handleHowMany(line);
		}
		else if(type == GlobalConstants.LineType.QUESTION_OF_TYPE_HOW_MUCH)
		{
			result =handleHowMuch(line);
		}
		return result;
	}
	
	private String handleHowMuch(String line)
	{
//		String regex = "^how much is ((?:\\w+ )+)\\?$";
		String regex = GlobalConstants.regexforHowMuchTypeQuestions;
		pattern = Pattern.compile(regex);
		Matcher m = pattern.matcher(line);

		//pish tegj glob glob
		m.matches();
		String  inputs =  m.group(1);
		
		if(galacticNumeral.areValid(inputs.split(" ")))
		{
			String romanNumeral = galacticRomanMapper.convertToRomanNumeral(inputs.split(" "));
			//XLII
			int number = rNumeral.evaluate(romanNumeral);
			if(number != -1 )
			{
				return inputs + " is " + number +".";
			}
			else
			{
				return null;
			}
		}
		
		return null;
	}
	
	private String handleHowMany(String line)
	{
		String regex = GlobalConstants.regexforHowManyTypeQuestions;
		pattern = Pattern.compile(regex);
		Matcher m = pattern.matcher(line);
		m.matches();
		
		String creditName = m.group(1);
		
		String [] galacticNumbers = m.group(2).split("\\s");
		String currencyName = m.group(3);
		
		
		if(!galacticNumeral.areValid(galacticNumbers))
		{
			PrintUtils.promptUser("Undeclared Galactic number was used, question ignored.");
			return null;
		}
		
		String romanNumerals = galacticNumeral.toRomanNumeral(galacticNumbers);
		int materialQuantitiy  = rNumeral.evaluate(romanNumerals);
		
		if(materialQuantitiy != -1)
		{
			double totalCredits = materialQuantitiy * metalCurrencyUnitMapper.getUnitValue(currencyName); 
			
			StringBuilder result = new StringBuilder();
			
			result.append(m.group(2)+  currencyName + " is " + totalCredits + " " + creditName);
			
			return result.toString();
		}
		return null;
	}
	
	
}
