package InterGallactic;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class Parser 
{
	public Parser()
	{
		
		inputScanner  = new Scanner(System.in);
		rNumeral = new RomanNumerals();
		galacticNumeral = new GalacticNumerals();
		galacticRomanMapper =  new GalacticRomanMapper(galacticNumeral);
		metalCurrencyUnitMapper = new MetalCurrencyUnitMapper(rNumeral, galacticNumeral);
		qHandler = new QuestionHandler(rNumeral, galacticNumeral, galacticRomanMapper, metalCurrencyUnitMapper);
	}
	
	private String [] getAssignmentOperands(String line)
	{
		String [] parts = line.split(" ");
		return new String [] { parts[0], parts[2]};
	}
	
	public void parse()
	{
		try
		{
			while(inputScanner.hasNext())
			{
					process(inputScanner.nextLine());
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			inputScanner.close();
		}
	}
	
	public void process(String lineContents)
	{
		String answer = "";
		LineRegEx[] lineRegEx = LineRegEx.values();
		boolean matchFound = false;

		if(lineContents.length() > 0)
		{
			for(LineRegEx l : lineRegEx)
			{
				matcher = l.getPattern().matcher(lineContents);

				if(matcher.matches())
				{
					switch(l.getType())
					{
					case ASSIGNMENT:
						// assignment processing.
						matchFound = true;
						String [] operands = getAssignmentOperands(lineContents);
						galacticRomanMapper.addToMap(operands[0], operands[1]);
						break;

					case CREDITSINFO:
						// CreditsInfo processing.
						matchFound = true;
						metalCurrencyUnitMapper.processCreditInfo(lineContents);
						break;

					case QUESTION_OF_TYPE_HOW_MANY:
						// How many types processing.
						matchFound = true;
						answer = qHandler.handle(lineContents, l.getType());
						if(answer != null)
						{
							PrintUtils.promptUser(answer);
						}
						break;

					case QUESTION_OF_TYPE_HOW_MUCH:
						// How much types processing.
						matchFound = true;
						answer = qHandler.handle(lineContents, l.getType());
						if(answer != null)
						{
							PrintUtils.promptUser(answer);
						}
						break;
					}
				}

				if(matchFound)
				{
					break;
				}
			}

			if(!matchFound)
			{
				PrintUtils.unSupportedInput();
			}
		}
	}
	
	private enum LineRegEx
	{
		Assignment(GlobalConstants.regexForAssignment, GlobalConstants.LineType.ASSIGNMENT),
		CreditsInfo(GlobalConstants.regexForCreditsInfo, GlobalConstants.LineType.CREDITSINFO),
		Question_HOW_MANY(GlobalConstants.regexforHowManyTypeQuestions, GlobalConstants.LineType.QUESTION_OF_TYPE_HOW_MANY),
		Question_HOW_MUCH(GlobalConstants.regexforHowMuchTypeQuestions, GlobalConstants.LineType.QUESTION_OF_TYPE_HOW_MUCH);
		
		private final Pattern linePattern;
		private final GlobalConstants.LineType type;
		LineRegEx(String linePattern, GlobalConstants.LineType type)
		{
			this.linePattern=Pattern.compile(linePattern);
			this.type = type;
		}
		
		Pattern getPattern()
		{
			return linePattern;
		}
		
		GlobalConstants.LineType getType()
		{
			return type;
		}
	}
	

	private Matcher matcher;
	
	private QuestionHandler qHandler;
	private GalacticRomanMapper galacticRomanMapper;
	private RomanNumerals rNumeral;
	private GalacticNumerals galacticNumeral;
	private MetalCurrencyUnitMapper metalCurrencyUnitMapper;
	private Scanner inputScanner;
		
}
