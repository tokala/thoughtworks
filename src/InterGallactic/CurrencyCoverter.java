package InterGallactic;

/*Assumptions made to write this program are----
1.All the questions will end with a " ?" , if not they are ignored.
 e.g: how much is pish tegj glob glob ?
2.Inputs are case sensitive 
 i.e "Silver" is not the same as "silver" ,  so is the case with "Credits".
3.Trading Metals and Dirt will have initial Letter as Capital
 e.g: "Gold","Iron","Organicmetrial"
4.All galactic numerals are small alphas
 e.g: pish
5.ASSIGNMENT statements have three words and fist word is galactic Numaral and last word is roman Numeral and middle word is "is"
6.Roman Numerals is one element of the following set {I(1), V(5), X(10), L(50), C(100), D(500),M(1000)} with values given along with them.
7.CREDITSINFO is info about credits in this format 
	romanNumerals Metal is Numeric units
8.QUESTION_OF_TYPE_HOW_MANY is of the following form "how many Units is romanNumerals Metal ?"
9.QUESTION_OF_TYPE_HOW_MUCH is of the following form "how much is romanNumerals Metal ?"
10.Any other input other than above type is unSupportedInput

About Classes
*/
public class CurrencyCoverter
{
	public static void main(String args [])
	{
		PrintUtils.promptUser("This program helps in currency conversion between Galactic Numerals and Roman Numerals." +
				"\n Feed in value for Roman Numerals like I,V,X etc..");
		Parser parser = new Parser();
		parser.parse();
	}
}
