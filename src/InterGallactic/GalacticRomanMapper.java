package InterGallactic;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

class GalacticRomanMapper 
{
	private GalacticNumerals galacticNumeralObj;
	
	private static Map<String, String> mappings;
	GalacticRomanMapper(GalacticNumerals galacticNumeral)
	{
		mappings = new HashMap<String, String>();
		this.galacticNumeralObj = galacticNumeral;
		galacticNumeralObj.storeNumeralMapper(this);
	}
	
	public String convertToRomanNumeral(String[] galacticNumeral)
	{
		StringBuilder romanNumeral = new StringBuilder(galacticNumeral.length);
		
		for(String s : galacticNumeral)
		{
			romanNumeral.append(mappings.get(s));
		}
		
		return romanNumeral.toString();
	}
	
	

	public void addToMap(String galacticNumeral, String rNumeral)
	{
		if( mappings.containsKey(galacticNumeral) || getKeyByValue(rNumeral) !=null)
		{
			PrintUtils.promptUser("Mapping is already present.");
		}
		else
		{
			if(galacticNumeralObj.isWord(galacticNumeral))
			{
				//add to list of know galactic numerals
				galacticNumeralObj.addNumeral(galacticNumeral);
				mappings.put(galacticNumeral, rNumeral);
			}
			else
			{
				PrintUtils.promptUser("Incorrect assignment.");
			}
		}
	}
	
	private String getKeyByValue(String value) {
	    for (Entry<String, String> entry : mappings.entrySet()) 
	    {
	        if (value.equals((String)(entry.getValue())))
	        {
	            return entry.getKey();
	        }
	    }
	    return null;
	}
	

}
