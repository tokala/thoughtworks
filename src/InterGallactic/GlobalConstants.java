package InterGallactic;

public class GlobalConstants
{
	public static enum LineType
	{
		ASSIGNMENT,
		CREDITSINFO,
		QUESTION_OF_TYPE_HOW_MANY,
		QUESTION_OF_TYPE_HOW_MUCH
	}
	
	static String regexForAssignment = "^([a-z]+) is ([I|V|X|L|C|D|M])$";
	static String regexForCreditsInfo = "((?:[a-z]+ )+)([A-Z]\\w+) is (\\d+) ([A-Z]\\w+)$";
	static String regexforHowManyTypeQuestions= "^how many ([a-zA-Z]\\w+) is ((?:\\w+ )+)([A-Z]\\w+) \\?$";
	static String regexforHowMuchTypeQuestions = "^how much is ((?:\\w+[^0-9] )+)\\?$";
}
