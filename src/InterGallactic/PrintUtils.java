package InterGallactic;

public class PrintUtils {

	public static void promptUser(String text) {
		System.out.println();
		System.out.println(text);
		System.out.println();
	}

	public static void unSupportedInput() {
		System.out.println();
		System.out.println("I have no idea what you are talking about.");
		System.out.println();
	}
}
