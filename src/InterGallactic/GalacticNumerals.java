package InterGallactic;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class GalacticNumerals 
{
	private static List<String> galacticNumerals;
	private GalacticRomanMapper galacticRomanMapper;
	public GalacticNumerals() 
	{
		galacticNumerals = new ArrayList<>();
	}
	
	public void storeNumeralMapper(GalacticRomanMapper galacticRomanMapper)
	{
		this.galacticRomanMapper = galacticRomanMapper;
	}
	
	public String toRomanNumeral(String [] numeral)
	{
		 return galacticRomanMapper.convertToRomanNumeral(numeral);
	}
	
	public void addNumeral(String numeral)
	{
		if(isWord(numeral))
		{
			galacticNumerals.add(numeral);
		}
		else
		{
			PrintUtils.promptUser("Inncorrect Galactic Numeral");
		}
	}
	
	private boolean isValid(String numeral)
	{
		for(String item : galacticNumerals)
		{
			if ( numeral.equals(item))
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean areValid(String [] numeral)
	{
		for (String s : numeral)
		{
			if(!isValid(s))
			{
				PrintUtils.promptUser(s +" is not a galactic Numeral.");
				return false;
			}
		}
		return true;
	}
	
	public boolean isWord(String numeral)
	{
		String wordRegex ="^[a-z]+";
		Matcher matcher;
		Pattern wordPattern = Pattern.compile(wordRegex);
		matcher = wordPattern.matcher(numeral);
		return (matcher.matches()? true: false);
	}
	

}
